// Dependencies
var express = require('express'),
    bodyParser = require('body-parser'),
    expressValidator = require('express-validator'),
    request = require("request"),
    validator = require('express-validator'),
    pug = require('pug'),
    path = require('path'),
    os = require('os'),
    _ = require('lodash');


// Middleware
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());


//Set up Template Engine Pug to Serve Dynamic HTML Templates
// Anything in ./public is served up as static content
var staticDir = path.join(__dirname, '/client');
// Anything in ./views are PUG templates
var viewsDir = __dirname + '/client/views';
app.use(express.static(staticDir));
// Configure the PUG view engine
app.set('view engine', 'pug');
app.set('views', viewsDir);


//Get Environment Variables and Set them to Local Variables


//Set Salesforce URL Based on Environment Variable
switch (process.env.NODE_ENV) {
    case 'production':
        console.log(process.env.NODE_ENV)
        break;
    case 'stage':
        console.log(process.env.NODE_ENV)
        break;
    case 'local':
    case 'dev':
        console.log(process.env.NODE_ENV)
        break;
    default:
        // code
}


//Set up Template Engine Pug to Serve Dynamic HTML Templates
// Anything in ./public is served up as static content
var staticDir = path.join(__dirname, '/client');
// Anything in ./views are PUG templates
var viewsDir = __dirname + '/client/views';
app.use(express.static(staticDir));
// Configure the PUG view engine
app.set('view engine', 'pug');
app.set('views', viewsDir);


app.get('/', function(req, res) {
    // res.redirect(oauth2.getAuthorizationUrl({ scope: 'full' }));
    var result = 'ENV:' + process.env.NODE_ENV;
    // res.end(JSON.stringify(result));
    res.render('home', { title: 'Welcome to Heroku Deploy Project: ', env: "ENV: " + process.env.NODE_ENV });
});

var server = app.listen(process.env.PORT || 8080, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});
